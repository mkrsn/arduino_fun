# Beethovens Für Elise

## Components Required
* 1x Arduino Uno R3 or equal
* 1x passive Buzzer
* 2x F-M Wire (Female to Male wires)

## Wiring
* Connect + to D8
* Connect - to GND

## Schematic
![Buzzer Schematic](https://gitlab.com/mkrsn/arduino_fun/raw/master/img/schematic_buzzer.png)
